package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.TSpecification;
import com.ruoyi.system.service.ITSpecificationService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 规格Controller
 * 
 * @author ruoyi
 * @date 2025-02-27
 */
@RestController
@RequestMapping("/specification")
public class TSpecificationController extends BaseController
{
    @Autowired
    private ITSpecificationService tSpecificationService;

    /**
     * 查询规格列表
     */
    @RequiresPermissions("system:specification:list")
    @GetMapping("/list")
    public TableDataInfo list(TSpecification tSpecification)
    {
        startPage();
        List<TSpecification> list = tSpecificationService.selectTSpecificationList(tSpecification);
        return getDataTable(list);
    }

    /**
     * 导出规格列表
     */
    @RequiresPermissions("system:specification:export")
    @Log(title = "规格", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TSpecification tSpecification)
    {
        List<TSpecification> list = tSpecificationService.selectTSpecificationList(tSpecification);
        ExcelUtil<TSpecification> util = new ExcelUtil<TSpecification>(TSpecification.class);
        util.exportExcel(response, list, "规格数据");
    }

    /**
     * 获取规格详细信息
     */
    @RequiresPermissions("system:specification:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tSpecificationService.selectTSpecificationById(id));
    }

    /**
     * 新增规格
     */
    @RequiresPermissions("system:specification:add")
    @Log(title = "规格", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TSpecification tSpecification)
    {
        return toAjax(tSpecificationService.insertTSpecification(tSpecification));
    }

    /**
     * 修改规格
     */
    @RequiresPermissions("system:specification:edit")
    @Log(title = "规格", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TSpecification tSpecification)
    {
        return toAjax(tSpecificationService.updateTSpecification(tSpecification));
    }

    /**
     * 删除规格
     */
    @RequiresPermissions("system:specification:remove")
    @Log(title = "规格", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tSpecificationService.deleteTSpecificationByIds(ids));
    }
}
