package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TSpecificationMapper;
import com.ruoyi.system.domain.TSpecification;
import com.ruoyi.system.service.ITSpecificationService;

/**
 * 规格Service业务层处理
 * 
 * @author ruoyi
 * @date 2025-02-27
 */
@Service
public class TSpecificationServiceImpl implements ITSpecificationService 
{
    @Autowired
    private TSpecificationMapper tSpecificationMapper;

    /**
     * 查询规格
     * 
     * @param id 规格主键
     * @return 规格
     */
    @Override
    public TSpecification selectTSpecificationById(Long id)
    {
        return tSpecificationMapper.selectTSpecificationById(id);
    }

    /**
     * 查询规格列表
     * 
     * @param tSpecification 规格
     * @return 规格
     */
    @Override
    public List<TSpecification> selectTSpecificationList(TSpecification tSpecification)
    {
        return tSpecificationMapper.selectTSpecificationList(tSpecification);
    }

    /**
     * 新增规格
     * 
     * @param tSpecification 规格
     * @return 结果
     */
    @Override
    public int insertTSpecification(TSpecification tSpecification)
    {
        return tSpecificationMapper.insertTSpecification(tSpecification);
    }

    /**
     * 修改规格
     * 
     * @param tSpecification 规格
     * @return 结果
     */
    @Override
    public int updateTSpecification(TSpecification tSpecification)
    {
        return tSpecificationMapper.updateTSpecification(tSpecification);
    }

    /**
     * 批量删除规格
     * 
     * @param ids 需要删除的规格主键
     * @return 结果
     */
    @Override
    public int deleteTSpecificationByIds(Long[] ids)
    {
        return tSpecificationMapper.deleteTSpecificationByIds(ids);
    }

    /**
     * 删除规格信息
     * 
     * @param id 规格主键
     * @return 结果
     */
    @Override
    public int deleteTSpecificationById(Long id)
    {
        return tSpecificationMapper.deleteTSpecificationById(id);
    }
}
