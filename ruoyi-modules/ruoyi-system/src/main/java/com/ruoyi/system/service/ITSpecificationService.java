package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.TSpecification;

/**
 * 规格Service接口
 * 
 * @author ruoyi
 * @date 2025-02-27
 */
public interface ITSpecificationService 
{
    /**
     * 查询规格
     * 
     * @param id 规格主键
     * @return 规格
     */
    public TSpecification selectTSpecificationById(Long id);

    /**
     * 查询规格列表
     * 
     * @param tSpecification 规格
     * @return 规格集合
     */
    public List<TSpecification> selectTSpecificationList(TSpecification tSpecification);

    /**
     * 新增规格
     * 
     * @param tSpecification 规格
     * @return 结果
     */
    public int insertTSpecification(TSpecification tSpecification);

    /**
     * 修改规格
     * 
     * @param tSpecification 规格
     * @return 结果
     */
    public int updateTSpecification(TSpecification tSpecification);

    /**
     * 批量删除规格
     * 
     * @param ids 需要删除的规格主键集合
     * @return 结果
     */
    public int deleteTSpecificationByIds(Long[] ids);

    /**
     * 删除规格信息
     * 
     * @param id 规格主键
     * @return 结果
     */
    public int deleteTSpecificationById(Long id);
}
