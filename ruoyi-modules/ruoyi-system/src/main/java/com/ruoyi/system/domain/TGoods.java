package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 商品对象 t_goods
 * 
 * @author ruoyi
 * @date 2025-02-27
 */
public class TGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 序号 */
    private Long id;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String name;

    /** 内存容量 */
    @Excel(name = "内存容量")
    private String memoryCapacity;

    /** 硬盘容量 */
    @Excel(name = "硬盘容量")
    private String hardDiskCapacity;

    /** 显存容量 */
    @Excel(name = "显存容量")
    private String videoMemoryCapacity;

    /** 品牌 */
    @Excel(name = "品牌")
    private String brand;

    /** 系列 */
    @Excel(name = "系列")
    private String series;

    /** CPU */
    @Excel(name = "CPU")
    private String cpu;

    /** 厚度 */
    @Excel(name = "厚度")
    private String thickness;

    /** 颜色分类 */
    @Excel(name = "颜色分类")
    private String colorType;

    /** 显卡类型 */
    @Excel(name = "显卡类型")
    private String videoCardType;

    /** 屏幕尺寸 */
    @Excel(name = "屏幕尺寸")
    private String dimension;

    /** 分辨率 */
    @Excel(name = "分辨率")
    private String resolution;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setMemoryCapacity(String memoryCapacity) 
    {
        this.memoryCapacity = memoryCapacity;
    }

    public String getMemoryCapacity() 
    {
        return memoryCapacity;
    }
    public void setHardDiskCapacity(String hardDiskCapacity) 
    {
        this.hardDiskCapacity = hardDiskCapacity;
    }

    public String getHardDiskCapacity() 
    {
        return hardDiskCapacity;
    }
    public void setVideoMemoryCapacity(String videoMemoryCapacity) 
    {
        this.videoMemoryCapacity = videoMemoryCapacity;
    }

    public String getVideoMemoryCapacity() 
    {
        return videoMemoryCapacity;
    }
    public void setBrand(String brand) 
    {
        this.brand = brand;
    }

    public String getBrand() 
    {
        return brand;
    }
    public void setSeries(String series) 
    {
        this.series = series;
    }

    public String getSeries() 
    {
        return series;
    }
    public void setCpu(String cpu) 
    {
        this.cpu = cpu;
    }

    public String getCpu() 
    {
        return cpu;
    }
    public void setThickness(String thickness) 
    {
        this.thickness = thickness;
    }

    public String getThickness() 
    {
        return thickness;
    }
    public void setColorType(String colorType) 
    {
        this.colorType = colorType;
    }

    public String getColorType() 
    {
        return colorType;
    }
    public void setVideoCardType(String videoCardType) 
    {
        this.videoCardType = videoCardType;
    }

    public String getVideoCardType() 
    {
        return videoCardType;
    }
    public void setDimension(String dimension) 
    {
        this.dimension = dimension;
    }

    public String getDimension() 
    {
        return dimension;
    }
    public void setResolution(String resolution) 
    {
        this.resolution = resolution;
    }

    public String getResolution() 
    {
        return resolution;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("memoryCapacity", getMemoryCapacity())
            .append("hardDiskCapacity", getHardDiskCapacity())
            .append("videoMemoryCapacity", getVideoMemoryCapacity())
            .append("brand", getBrand())
            .append("series", getSeries())
            .append("cpu", getCpu())
            .append("thickness", getThickness())
            .append("colorType", getColorType())
            .append("videoCardType", getVideoCardType())
            .append("dimension", getDimension())
            .append("resolution", getResolution())
            .toString();
    }
}
